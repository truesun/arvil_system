var sizeList, moduleDataList, keyList, keyVal;
$(function () {
	//關閉任何的lightbox
	$("#mask,.lg-bot .btn-cancel,.lg-msgBtn").on("click", function () {
		$("#mask,.lg-box").fadeOut(300);
	});

	//打開lightbox
	$("#btnOpenMesg").on("click", function () {
		$("#lgMesg,#mask").fadeIn(300);
	});
	$("#btnOpenBig").on("click", function () {
		$("#lgBig,#mask").fadeIn(300);
	});
	$("#addModuleBtn").on("click", function () {
		$("#moduleLgList,#mask").fadeIn(300);
	});

	//=== 加入工程列表 ===
	// hasKeyList = [];
	//保留記住價格資料
	keyVal = [];
	//有的SIZE列表
	sizeList = [];
	$('#sizeArea').find('input').on('change', function () {
		var isCheck = $(this).is(':checked');
		var sizeTxt = $(this).parent().find('span').html();
		if (isCheck) {
			sizeList.push(sizeTxt);
		} else {
			var ind = sizeList.findIndex(function (item) {
				return item == sizeTxt;
			});
			sizeList.splice(ind, 1);
		}
		// console.log(sizeList);
	});
	//選擇工程加入
	moduleDataList = [];
	$('#moduleItemList').find('input').on('change', function () {
		var isCheck = $(this).is(':checked');
		var itemId = $(this).attr('id');
		var itemTxt = $(this).parent().find('span').html();
		if (isCheck) {
			moduleDataList.push({ 'id': itemId, 'name': itemTxt });
		} else {
			var ind = moduleDataList.findIndex(function (item) {
				return item.id == itemId;
			});
			moduleDataList.splice(ind, 1);
		}
		// console.log(moduleDataList);
	});
	$('#addModuleListBtn').on('click', function () {
		// console.log(sizeList);
		// console.log(moduleDataList);
		if (sizeList.length && moduleDataList.length) {
			//更新至列表
			updateModuleList();
			//加入後清除
			sizeList = [];
			moduleDataList = [];
			$('#sizeArea').find('input').prop('checked', false);
			$('#moduleItemList').find('input').prop('checked', false);
		} else {
			if (!sizeList.length) {
				openLgMsg('請選擇尺寸');
			} else if (!moduleDataList.length) {
				openLgMsg('請選擇工程');
			}
		}
	});
	//=== 移除資料 ===
	$(document).on('click', '.del-btn', function (ind, item) {
		$(this).parent().parent().find('input').each(function (ind, item) {
			var delInputId = $(item).attr('id');
			// console.log(delInputId);
			var delInd = keyVal.findIndex(function (item) {
				return item.id == delInputId;
			});
			// console.log(delInd);
			//刪掉有的
			keyVal.splice(delInd, 1);
		});
		//移除畫面
		$(this).parent().parent().remove();
		var $moduleList = $('#moduleList');
		var listNum = $moduleList.find('li').length;
		if (!listNum) {
			$moduleList.html('<li class="no-data-txt" id="moduleNoData">尚無加入尺寸或工程。</li>');
		}
	});

	//=== 0108 修改功能  ===
	//=== 貨號單編輯尺寸  ===
	// 選擇 sizeArea 尺寸
	var $orderList = $('#orderList');
	$('#sizeArea').find('input').each(function (ind, item) {
		$(this).on('change', function () {
			var isC = $(this).is(':checked');
			var isCVal = $(this).attr('id');
			console.log(isC);
			if (isC) {
				$orderList.append('<li>' +
					'<div class= "group-box-100" >' +
					'<div class="box-15 size-id">' + isCVal + '</div>' +
					'<div class="box-20 txt-c">' +
					'<input type="text" class="input-sty">' +
					'</div>' +
					'</div>' +
					'</li>');
			} else {
				$orderList.find('.size-id').each(function (ind, item) {
					var sizeId = $(this).html();
					if (sizeId == isCVal) {
						$(this).parent().parent().remove();
					}
				});
			}
			//如果無資料 移除
			var listNum = $orderList.find('li').length;
			if (!listNum) {
				$orderList.html('<li class="no-data-txt" id="orderNoData">尚無選擇尺寸。</li>');
			} else {
				$('#orderNoData').remove();
			}
		});
	});
	//=== 模組編輯工程  ===
	var $moduleSizeList = $('#moduleSizeList');
	$('#moduleItemList').find('input').each(function (ind, item) {
		$(this).on('change', function () {
			var isC = $(this).is(':checked');
			var isCVal = $(this).parent().find('span').html();
			console.log(isCVal);
			if (isC) {
				$moduleSizeList.append('<li>' +
					'<div class= "group-box-100" >' +
					'<div class="box-30 size-id">' + isCVal + '</div>' +
					'<div class="box-20 txt-c">' +
					'<input type="text" class="input-sty">' +
					'</div>' +
					'</div>' +
					'</li>');
			} else {
				$moduleSizeList.find('.size-id').each(function (ind, item) {
					var sizeId = $(this).html();
					if (sizeId == isCVal) {
						$(this).parent().parent().remove();
					}
				});
			}
			//如果無資料 移除
			var listNum = $moduleSizeList.find('li').length;
			if (!listNum) {
				$moduleSizeList.html('<li class="no-data-txt" id="moduleSizeNoData">尚無加入工程。</li>');
			} else {
				$('#moduleSizeNoData').remove();
			}
		});
	});

	//===== 舊的 =====
	// $('#sizeArea').find('input').on('change', function () {
	// 	var isCheck = $(this).is(':checked');
	// 	var sizeTxt = $(this).parent().find('span').html();
	// 	if (isCheck) {
	// 		sizeList.push(sizeTxt);
	// 		updateModuleList();
	// 	} else {
	// 		var ind = sizeList.findIndex(function (item) {
	// 			return item == sizeTxt;
	// 		});
	// 		sizeList.splice(ind, 1);
	// 		//如果size移除，列表移除這個size元素
	// 		// $('#moduleList').find('.sizeTxt').each(function (ind, item) {
	// 		// 	var removeTxt = $(this).html();
	// 		// 	if (removeTxt == sizeTxt) {
	// 		// 		$(this).parent().parent().remove();
	// 		// 	}
	// 		// });
	// 		// $('#moduleList').find('.sizeTxt').parent().parent().remove();
	// 		updateModuleList();
	// 	}
	// 	//如果都沒有勾Size
	// 	if (!sizeList.length || !moduleDataList.length) {
	// 		var $moduleList = $('#moduleList');
	// 		$moduleList.html('<li class="no-data-txt">尚無加入尺寸或工程。</li>');
	// 	}
	// });
	//選擇工程加入
	// moduleDataList = [];
	// moduleDataList = [{ 'id': 'm1', 'name': '罩杯付' }];
	// $('#addModuleToBtn').on('click', function () {
	// 	moduleDataList = [];
	// 	$('#moduleItemList').find('input:checked').each(function (ind, item) {
	// 		var itemId = $(item).attr('id');
	// 		var itemTxt = $(item).parent().find('span').html();
	// 		moduleDataList.push({ 'id': itemId, 'name': itemTxt });
	// 	});
	// 	console.log(moduleDataList);
	// 	//工程列表顯示
	// 	var $moduleList = $('#moduleList');
	// 	if (!moduleDataList.length) {
	// 		$moduleList.html('<li class="no-data-txt">尚無加入尺寸或工程。</li>');
	// 	} else {
	// 		updateModuleList();
	// 	}
	// 	//關閉工程ligjtBox
	// 	$("#moduleLgList,#mask").fadeOut(300);
	// });

	//移除工程 舊的
	// $(document).on('click', '.del-btn', function (ind, item) {
	// 	//移除資料
	// 	$(this).parent().parent().find('input').each(function (ind, item) {
	// 		var delInputId = $(item).attr('id');
	// 		var delInd = keyVal.findIndex(function (item) {
	// 			return item.id == delInputId;
	// 		});
	// 		//如果有在刪掉
	// 		if (delInd !== -1) {
	// 			keyVal.splice(delInd, 1);
	// 		}
	// 	});
	// 	// console.log('刪除後的儲存值');
	// 	// console.log(keyVal);
	// 	//移除畫面
	// 	$(this).parent().parent().remove();
	// 	var $moduleList = $('#moduleList');
	// 	var listNum = $moduleList.find('li').length;
	// 	if (!listNum) {
	// 		$moduleList.html('<li class="no-data-txt">尚無加入尺寸或工程。</li>');
	// 	}
	// });

	//側邊欄選單
	$('#closeNav').on('click', function () {
		$('#closeNav').animate({ left: '-26px' }, 300);
		$('.side-nav').animate({ width: '0' }, 300);
		$('.btn-side-open').animate({ left: '0' }, 400);
	});

	$('#openNav').on('click', function () {
		$('#closeNav').animate({ left: '180px' }, 300);
		$('.side-nav').animate({ width: '180px' }, 300);
		$('.btn-side-open').animate({ left: '-26px' }, 400);
	});

	//上傳檔案
	$('#uploadFile').change(function () {
		var filename = $(this).val();
		var lastIndex = filename.lastIndexOf("\\");
		if (lastIndex >= 0) {
			filename = filename.substring(lastIndex + 1);
		}
		$('#fileNameBox').text(filename);
	});

	//增加標籤
	$('#btnTagAdd').on('click', function () {
		$('#tagCreat').show();
	});
	$('#btnTagAdd2').on('click', function () {
		$('#tagCreat2').show();
	});

});
// $(window).load(function (e) {
// 	//側邊欄nav height
// 	sideNav();
// })
// $(window).resize(function () {
// 	sideNav();
// });

//上傳圖片的按鈕
function upload_click(obj) {
	var fileEvent = $(obj).parent().find('input[type=file]');
	fileEvent.click();
}

//側邊欄nav height
// function sideNav() {
// 	var _navH = $('.side-nav').height();
// 	var _logo = $('.side-logo').outerHeight();
// 	var _tit = $('.backend-tit').outerHeight();
// 	var _info = $('.user-info').outerHeight();
// 	var _sideFoot = $('.side-footer').outerHeight();
// 	//console.log( _navH,_logo,_tit,_info,_sideFoot);
// 	$('.nav-box').height(_navH - (_logo + _tit + _info + _sideFoot + 50));
// }
//lightBox提醒
function openLgMsg(txt) {
	$("#lgMesg,#mask").fadeIn(300);
	$('#lgMesg').find('.lg-cont').html(txt)
}

//更新工程列表
function updateModuleList() {
	var $moduleList = $('#moduleList');
	// $moduleList.html('');
	$('#moduleNoData').remove();
	$.each(moduleDataList, function (ind, val) {
		var moduleId = val.id;
		var moduleTxt = val.name;
		$.each(sizeList, function (ind, val) {
			// var index = ind + 1;
			var sizeTxt = val;
			var nt = 'D' + moduleId + sizeTxt;//單價
			var num = 'N' + moduleId + sizeTxt;//件數
			// var hasItem = keyVal.indexOf(nt);
			var keyind = keyVal.findIndex(function (item) {
				return item.id == nt;
			});
			// console.log(keyind);
			if (keyind < 0) {
				keyVal.push({ 'id': nt, 'val': '' }, { 'id': num, 'val': '' });
				$moduleList.append('<li> ' +
					'<div class="group-box-83"> ' +
					'<div class="box-30">' + moduleTxt + '</div> ' +
					'<div class="box-15">' + sizeTxt + '</div> ' +
					'<div class="box-20 txt-c"> ' +
					'<input type="text" class="input-sty" id="' + nt + '" name="' + nt + '"> ' +
					'</div> ' +
					'<div class="box-20 txt-c"> ' +
					'<input type="text" class="input-sty" id="' + num + '" name="' + num + '"> ' +
					'</div> ' +
					'</div> ' +
					'<div class="group-box-15 opera"> ' +
					'<a class="del-btn" title="刪除" alt="刪除"> ' +
					'<i class="fad fa-times-square"></i> ' +
					'</a> ' +
					'</div> ' +
					'</li>');
			}
			console.log(keyVal);
		});
	});
	//=== 如果有加入就要預先記著 ===
	$moduleList.find('input').each(function (ind, item) {
		// var sid = $(this).attr('id');
		//將列表有的input ID 記下來
		// keyList.push(sid);
		$(this).on('change', function () {
			var id = $(this).attr('id');
			var val = $(this).val();
			//如果有輸入價格，先記住
			var keyind = keyVal.findIndex(function (item) {
				return item.id == id;
			});
			keyVal[keyind].val = val;
			// console.log('輸入後的記住值', keyVal);
		});
	});
	//== 加入改變後的列表顯示狀態 舊的 ==
	// keyList = [];//出現的input
	// $moduleList.find('input').each(function (ind, item) {
	// 	var sid = $(this).attr('id');
	// 	//將列表有的input ID 記下來
	// 	keyList.push(sid);
	// 	$(this).on('change', function () {
	// 		var id = $(this).attr('id');
	// 		var val = $(this).val();
	// 		//如果有輸入價格，先記住
	// 		var keyind = keyVal.findIndex(function (item) {
	// 			return item.id == id;
	// 		});
	// 		if (keyind == -1) {//如果沒有加入
	// 			keyVal.push({ 'id': id, 'val': val });
	// 		} else if (keyind !== -1) {//如果有就更新
	// 			keyVal[keyind].val = val;
	// 		}
	// 		console.log('輸入後的記住值', keyVal);
	// 	});
	// });
	// console.log('列表中的input', keyList);

	//== 這次缺少的input沒有近來，要將預設記住資料移除 舊的 ==
	// if (keyList.length) {
	// 	var hasList = [];
	// 	$.each(keyList, function (ind, val) {
	// 		var hasId = val;
	// 		if (keyVal.length) {
	// 			var hasData = keyVal.filter(function (item) {
	// 				return item.id == hasId;// { 'uid': '001', 'name': 'simple' }
	// 			});
	// 			// console.log('篩選出來的');
	// 			// console.log(hasData);
	// 			if (hasData.length) {
	// 				hasList.push(hasData[0]);
	// 			}
	// 		}
	// 	});
	// 	// console.log('篩選出來的');
	// 	// console.log(hasList);
	// 	//篩選出來後塞入預計列表值
	// 	keyVal = hasList;
	// }

	// console.log('有的值～～：', keyVal);
	//== 如果有記住的價格，要填入  舊的 ==
	// if (keyVal.length) {
	// 	$.each(keyVal, function (ind, val) {
	// 		var $item = $('#' + val.id);
	// 		var itemVal = val.val;
	// 		$item.val(itemVal);
	// 	});
	// }
}

